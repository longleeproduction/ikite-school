//
//  StudentViewController.h
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudentModel.h"

@interface StudentViewController : UIViewController
@property(nonatomic, strong) StudentModel *model;
@end
