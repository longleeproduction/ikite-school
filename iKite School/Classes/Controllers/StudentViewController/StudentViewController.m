//
//  StudentViewController.m
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import "StudentViewController.h"

@interface StudentViewController () {
    
    __weak IBOutlet UIImageView *avatarImageView;
    __weak IBOutlet UITextField *firstNameTextField;
    
    __weak IBOutlet UITextField *lastNameTextField;
    
    __weak IBOutlet UITextField *phoneTextField;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *addressTextField;
    
}

@end

@implementation StudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    PFFile *avatarFile = [self.model objectForKey:@"avatar"];
    [avatarFile getDataInBackgroundWithBlock:^(NSData * _Nullable data, NSError * _Nullable error) {
        if (!error) {
            [avatarImageView setImage:[UIImage imageWithData:data]];
        }
    }];
    [firstNameTextField setText:[self.model objectForKey:@"firstName"]];
    [lastNameTextField setText:[self.model objectForKey:@"lastName"]];
    [phoneTextField setText:[self.model objectForKey:@"phone"]];
    [emailTextField setText:[self.model objectForKey:@"email"]];
    [addressTextField setText:[self.model objectForKey:@"address"]];
    
    [self setEditTextFieldEnable:NO];
}
-(void)setEditTextFieldEnable:(BOOL)boo {
    firstNameTextField.enabled = lastNameTextField.enabled = phoneTextField.enabled = emailTextField.enabled = addressTextField.enabled = boo;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)touchEdit:(id)sender {
    [self setEditTextFieldEnable:YES];
}



@end
