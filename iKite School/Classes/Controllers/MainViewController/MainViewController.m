//
//  MainViewController.m
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import "MainViewController.h"
#import "MainTableViewCell.h"
#import "StudentViewController.h"
#import "StudentQuery.h"
#import "StudentModel.h"
#import "StudentAddViewController.h"

@interface MainViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate>{
    
    
    __weak IBOutlet UIButton *studentAddButton;
    __weak IBOutlet UILabel *studentHomeLabel;
    __weak IBOutlet UIView *navigationView;
    __weak IBOutlet UIView *studenSearchView;
    __weak IBOutlet UITableView *studentTableView;
    
    StudentQuery *studentQuery;
    int pageLoad;
    NSMutableArray *studentList;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [studentTableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"MainTableViewCell"];
    studentTableView.delegate = self;
    studentTableView.allowsSelectionDuringEditing = YES;
    studentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self setupData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addStudent" object:nil];
}


#pragma mark - Setup Data
-(void)setupData {
    studentList = [NSMutableArray new];
    pageLoad = 0;
    
    studentQuery = [StudentQuery shareInstance];
    [studentQuery getStudentAtPage:pageLoad finishSucess:^(NSArray *students) {
        [studentList addObjectsFromArray:students];
        [studentTableView reloadData];
    } finishFail:^(NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Opps!!!" message:@"Đã xảy ra lỗi khi lấy dữ liệu." delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles: nil];
        [alert show];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedStudent:) name:@"addStudent" object:nil];
}
-(void)receivedStudent:(NSNotification *)notification{
    NSDictionary *dict = [notification userInfo];
    StudentModel *model = (StudentModel *)[dict objectForKey:@"student"];
    if (model) {
        [studentList insertObject:model atIndex:0];
        [studentTableView reloadData];
    }
}


#pragma mark - Student Table view delegate + data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return studentList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainTableViewCell *cell = [studentTableView dequeueReusableCellWithIdentifier:@"MainTableViewCell"];
    
    [cell configCellWithObject:[studentList objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    StudentViewController *student = [StudentViewController new];
    student.model = [studentList objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:student animated:YES];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}



#pragma mark - Touch Add Student
/**
 *  Touch Add student button --> Push View Add
 *
 *  @param sender nil
 */
- (IBAction)touchAddStudent:(id)sender {
    StudentAddViewController *add = [StudentAddViewController new];
    [self.navigationController pushViewController:add animated:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - SCrollDelegate
-(void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    NSLog(@"Refresh User");
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

@end
