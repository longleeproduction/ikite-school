//
//  StudentAddViewController.m
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import "StudentAddViewController.h"
#import "StudentModel.h"
#import "StudentQuery.h"
#import "ValidateString.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface StudentAddViewController ()<UIAlertViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    
    __weak IBOutlet UIImageView *avatarImageView;
    
    __weak IBOutlet UIButton *avatarButton;
    __weak IBOutlet UITextField *firstNameTextField;
    
    __weak IBOutlet UITextField *lastNameTextField;
    __weak IBOutlet UITextField *phoneTextFeild;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *addressTextField;
    
    __weak IBOutlet UIButton *addButton;
    __weak IBOutlet UIActivityIndicatorView *indicator;
    
    UIImagePickerController *picker;
    NSData *imageData;



}

@end

@implementation StudentAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    indicator.hidden = YES;
    picker = [[UIImagePickerController alloc] init];
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2;
    avatarImageView.backgroundColor = [UIColor clearColor];
    avatarButton.backgroundColor = [UIColor clearColor];
    avatarButton.layer.cornerRadius = avatarButton.frame.size.height/2;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Touch Event

- (IBAction)chooseAvatar:(id)sender {
    UIAlertView *alertChooseImage = [[UIAlertView alloc]initWithTitle:@"Choose Image" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Photo Library",@"Camera", nil];
    [alertChooseImage show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        NSLog(@"Choose image from library");
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }else if (buttonIndex == 2) {
        NSLog(@"Choose image from camera");
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

#pragma mark - ImagePickerController
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    UIImage *imageFinal = nil;
    
    // Get extension Image
    NSURL *linkImage = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSString *path = [linkImage path];
    NSString *extension = [path pathExtension];
    
    // Image Edit -- Crop
    if ([mediaType isEqualToString:(__bridge NSString*)kUTTypeImage]) {
        imageFinal = [info objectForKey:UIImagePickerControllerEditedImage];
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        if (imageFinal) {
            NSLog(@"Choose image success %@ ", extension);
            [avatarImageView setImage:imageFinal];
            if([extension isEqualToString:@"PNG"]){
                imageData = UIImagePNGRepresentation(imageFinal);
            } else {
                imageData = UIImageJPEGRepresentation(imageFinal, 0.0);
            }
        }
    }];
}



- (IBAction)touchAddStudent:(id)sender {
    
    if ([firstNameTextField.text isEqualToString:@""] || [lastNameTextField.text isEqualToString:@""] || [emailTextField.text isEqualToString:@""] || [addressTextField.text isEqualToString:@""] || [phoneTextFeild.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Lỗi" message:@"Vui lòng nhập đầy đủ thông tin sinh viên." delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (![ValidateString IsValidEmail:emailTextField.text]) {
        [emailTextField becomeFirstResponder];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Lỗi" message:@"Email nhập vào không đúng." delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (imageData == nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Lỗi" message:@"Chọn ảnh đại diện." delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    addButton.hidden = YES;
    indicator.hidden = NO;
    [indicator startAnimating];
    self.view.userInteractionEnabled = NO;
    PFFile *avatar = [PFFile fileWithName:[NSString stringWithFormat:@"%@-%@",emailTextField.text, phoneTextFeild.text] data:imageData];
    [avatar saveInBackground];
    
    StudentModel *student = [StudentModel shareInstance];
    [student setValue:firstNameTextField.text forKey:@"firstName"];
    [student setValue:lastNameTextField.text forKey:@"lastName"];
    [student setValue:emailTextField.text forKey:@"email"];
    [student setValue:addressTextField.text forKey:@"address"];
    [student setValue:phoneTextFeild.text forKey:@"phone"];
    [student setValue:avatar forKey:@"avatar"];
    
        [student saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            addButton.hidden = NO;
            indicator.hidden = YES;
            [indicator stopAnimating];
            self.view.userInteractionEnabled = YES;
            if (succeeded) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thành công" message:@"Sinh viên đã được thêm vào danh sách" delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles: nil];
                [alert show];
                // Delegate back with student User
                [[NSNotificationCenter defaultCenter] postNotificationName:@"addStudent" object:nil userInfo:@{@"student":student}];
                
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Lỗi" message:@"Đã xảy ra lỗi khi thêm sinh viên" delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles: nil];
                [alert show];
            }
        }];
}
- (IBAction)touchBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}






@end
