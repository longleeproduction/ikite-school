//
//  StudentModel.m
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import "StudentModel.h"

@implementation StudentModel {
    NSMutableArray *students;
}
@synthesize phone, firstName, lastName, email, address, objectId, avatar;


+(StudentModel *)shareInstance {
    static StudentModel *studentModel = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        studentModel = (StudentModel *)[PFObject objectWithClassName:@"Student"];
    });
    return studentModel;
}

@end
