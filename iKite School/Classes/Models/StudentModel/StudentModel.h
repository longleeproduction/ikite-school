//
//  StudentModel.h
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import <Parse/Parse.h>

@interface StudentModel : PFObject
@property(nonatomic) NSString* objectId;
@property(nonatomic) NSString* firstName;
@property(nonatomic) NSString* lastName;
@property(nonatomic) NSString* email;
@property(nonatomic) NSString* phone;
@property(nonatomic) NSString* address;
@property(nonatomic) PFFile* avatar;
+(StudentModel *)shareInstance;

@end
