//
//  StudentQuery.h
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import <Parse/Parse.h>

@interface StudentQuery : NSObject

@property(nonatomic, strong) PFQuery *query;

+(StudentQuery *)shareInstance;

-(void)getStudentAtPage:(int)page finishSucess:(void(^)(NSArray *students))sucess finishFail:(void(^)(NSError *error))fail;
@end
