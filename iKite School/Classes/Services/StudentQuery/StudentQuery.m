//
//  StudentQuery.m
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import "StudentQuery.h"

@implementation StudentQuery

+(StudentQuery *)shareInstance {
    static StudentQuery *studentQuery = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        studentQuery = [[StudentQuery alloc] init];
        studentQuery.query = [PFQuery queryWithClassName:@"Student"];
        studentQuery.query.limit = 20;
    });
    return studentQuery;
}

-(void)getStudentAtPage:(int)page finishSucess:(void (^)(NSArray *))sucess finishFail:(void (^)(NSError *))fail {
    StudentQuery *studentQuery = [StudentQuery shareInstance];
    studentQuery.query.skip = page*20;
    [studentQuery.query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (error) {
            fail(error);
        } else {
            sucess(objects);
        }
    }];
}

@end
