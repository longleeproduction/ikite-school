//
//  ValidateString.h
//  iKite School
//
//  Created by Lee Long on 12/24/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidateString : NSObject
+(BOOL) IsValidEmail:(NSString *) string;
+(BOOL) IsValidPhoneNumber:(NSString *) phone;
@end
