//
//  MainTableViewCell.m
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import "MainTableViewCell.h"
#import <Parse/Parse.h>

@implementation MainTableViewCell {

    __weak IBOutlet UIImageView *avatarImageView;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *phoneLabel;
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet UILabel *addressLabel;


}

- (void)awakeFromNib {
    avatarImageView.backgroundColor = [UIColor clearColor];
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString*)identifier {
    return NSStringFromClass([self class]);
}

-(void)configCellWithObject:(PFObject *)object {
    StudentModel *student = (StudentModel *)object;
    PFFile *file = [student objectForKey:@"avatar"];
    
    [file getDataInBackgroundWithBlock:^(NSData * _Nullable data, NSError * _Nullable error) {
        if (!error) {
            avatarImageView.image = [UIImage imageWithData:data];
        }
    }];
    
    nameLabel.text = [NSString stringWithFormat:@"%@ %@", [student objectForKey:@"firstName"], [student objectForKey:@"lastName"]];
    phoneLabel.text = [student objectForKey:@"phone"];
    emailLabel.text = [student objectForKey:@"email"];
    addressLabel.text = [student objectForKey:@"address"];
    
}

@end
