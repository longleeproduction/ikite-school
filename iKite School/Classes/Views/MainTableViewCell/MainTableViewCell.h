//
//  MainTableViewCell.h
//  iKite School
//
//  Created by Lee Long on 12/23/15.
//  Copyright © 2015 Long Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "StudentModel.h"

@interface MainTableViewCell : UITableViewCell

+ (NSString*)identifier;
-(void)configCellWithObject:(PFObject *)object;

@end
